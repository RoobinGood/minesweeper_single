require(
	["jquery", "underscore"],
	function($, _) {

		var lowLevel = [10, 10, 15];
		var midLevel = [15, 15, 30];
		var hiLevel = [20, 20, 50];

		// MAP MODEL
		var initMap = function(m, n, minesCount) {
			var map = {};
			map.xCount = n;
			map.yCount = m; 
			map.minesCount = minesCount;
			map.openedCount = 0;
			map.flagedCount = 0;
			map.mines = [];
			map.tips = [];
			map.openedCells = [];
			map.flagedCells = [];

			for (var i=0; i<map.xCount; i++) {
				map.mines.push([]);
				map.openedCells.push([]);
				map.flagedCells.push([]);
				for (var j=0; j<map.yCount; j++) {
					map.mines[i].push(false);
					map.openedCells[i].push(false);
					map.flagedCells[i].push(false);
				}
			}

			console.log("init", map);
			return map;
		}

		var generateMineMap = function(map) {
			for (var i=0; i<map.minesCount; i++) {
				var planted = false;
				while (!planted) {
					var x = _.random(map.xCount-1);
					var y = _.random(map.yCount-1);

					planted = !map.mines[x][y];
					if (planted) {
						map.mines[x][y] = true;
					}
				}
			}
		};

		var generateTipsMap = function(map) {
			for (var i=0; i<map.xCount; i++) {
				map.tips.push([]);
				for (var j=0; j<map.yCount; j++) {
					var counter = 0;

					for (var x = i===0 ? i : i-1; x<map.xCount && x<=i+1; x++) {
						for (var y = j===0 ? j : j-1; y<map.yCount && y<=j+1; y++){
							map.mines[x][y] && counter++;
						}
					}

					map.tips[i].push(counter);
				}
			}
		};

		var expandOpenedArea = function(map, x, y) {
			// console.log("expand init", x, y);
			for (var k = x===0 ? x : x-1; k<map.xCount && k<=x+1; k++) {
				for (var l = y===0 ? y : y-1; l<map.yCount && l<=y+1; l++){
					map.mines[k][l] && counter++;
					// console.log("expand", k, y, map.tips[k][l]);

					if (!map.openedCells[k][l]) {
						map.openedCells[k][l] = true;
						if (map.tips[k][l] === 0) {
							expandOpenedArea(map, k, l);
						}
					}
				}
			}
		};

		var checkWin = function(map) {
			return map.openedCount === map.xCount*map.yCount - map.minesCount;
		};

		var updateOpenedCount = function(map) {
			var count = 0;
			for (var i=0; i<map.xCount; i++) {
				for (var j=0; j<map.yCount; j++) {
					map.openedCells[i][j] && count++;
				}
			}
			map.openedCount = count;
		};


		// MAP VIEW
		var initTable = function(map) {
			$("#gameStatusMessage").hide();
			var result = "<table id=\"mapTable\"><tbody>";

			for (var i=0; i<map.yCount; i++) {
				result += "<tr>";
				for (var j=0; j<map.xCount; j++) {
					result += ["<td class=\"mapCell\"", 
						" data-position-x=", j,
						" data-position-y=", i, 
						"><div class=\"mapEmptyCell\"",
						"><p class=\"verticalAlign\"></p></div>",
						"</td>"].join("");
				}
				result += "</tr>";
			}

			result += "</tbody></table>";

			return result;
		};

		var showOpenedCellsMap = function(map) {
			$("#mapTable").find(".mapCell").each(function() {
				var x = $(this).data("position-x");
				var y = $(this).data("position-y");

				if (map.openedCells[x][y]) {
					if (map.mines[x][y]) {
						$(this).find("div").removeClass("mapEmptyCell")
							.addClass("mapMineCell")
							.find("p").text("M");

					} else {
						if ($(this).find("div").hasClass("mapEmptyCell")) {
							var cellClass;
							if (map.tips[x][y] <= 1) {
								cellClass = "mapLowWarnCell";
							} else if (map.tips[x][y] <= 3) {
								cellClass = "mapMidWarnCell";
							} else {
								cellClass = "mapHiWarnCell";
							}

							$(this).find("div").removeClass("mapEmptyCell")
								.removeClass("mapFlagCell")
								.addClass(cellClass)
								.find("p").text(map.tips[x][y]);		
						}

					}
				} else if (map.flagedCells[x][y]) {
					$(this).find("div").removeClass("mapEmptyCell")
						.addClass("mapFlagCell")
						.find("p").text("C");
				} else {
					$(this).find("div").removeClass("mapLowWarnCell")
						.removeClass("mapMidWarnCell")
						.removeClass("mapHiWarnCell")
						.removeClass("mapFlagCell")
						.addClass("mapEmptyCell")
						.find("p").text("");
				}
			});
		};

		var showGameEnd = function(gameResult) {
			$("#gameStatusMessage").show().find("div")
				.removeClass("gameStatusWin")
				.removeClass("gameStatusLose");

			var className = gameResult ? 
				"gameStatusWin": "gameStatusLose";
			var text = gameResult ? 
				"YOU WIN" : "YOU LOSE";

			$("#gameStatusMessage").find("div")
				.addClass(className).next().text(text);
		};

		var showScore = function(map) {
			$("#score").find("h3").text([map.openedCount, ":",
				map.xCount*map.yCount, "//", map.flagedCount, 
				":", map.minesCount].join(" "))
		};


		// MAP CONTROLLER
		var map;
		var gameState = true;

		var initInfoBoard = function(map) {
			showScore(map);
		}

		var initGame = function(map, level) {
			gameState = true;
			map = initMap.apply(undefined, level);
			generateMineMap(map);
			generateTipsMap(map);

			$("#map").html(initTable(map));
			initInfoBoard(map);
			
			$(".mapCell").on("click", function(ev) {
				if (gameState) {
					var x = $(this).data("position-x");
					var y = $(this).data("position-y");
					console.log("click", y, x);
					
					if (!map.flagedCells[x][y]) {
						map.openedCells[x][y] = true;
						if (map.tips[x][y] === 0) {
							expandOpenedArea(map, x, y);
						}
						updateOpenedCount(map);

						showOpenedCellsMap(map);
						showScore(map);

						if (map.mines[x][y] || checkWin(map)) {
							gameState = false;
							showGameEnd(!map.mines[x][y]);
						}
					}
				} else {
					console.log("GAME OVER");
				}
			}).on("contextmenu", function(el) {
				el.preventDefault();
				console.log(el);

				if (gameState && map.flagedCount<map.minesCount) {
					console.log();
					var x = $(el.currentTarget).data("position-x");
					var y = $(el.currentTarget).data("position-y");
					console.log("check", y, x);

					map.flagedCells[x][y] = !map.flagedCells[x][y];
					if (map.flagedCells[x][y]) {
						map.flagedCount++;
					} else {
						map.flagedCount--;
					}

					showOpenedCellsMap(map);
					showScore(map);
				}
			});
		};

		var teardownGame = function(map) {
			$(".mapCell").off("click").off("contextmenu");
		}

		var currentLevel = lowLevel;
		var changeLevel = function(level) {
			teardownGame(map);
			currentLevel = level;
			initGame(map, currentLevel);
		}

		$("#lowMode").on("click", function(el) {
			changeLevel(lowLevel);
		});

		$("#midMode").on("click", function(el) {
			changeLevel(midLevel);
		});

		$("#hiMode").on("click", function(el) {
			changeLevel(hiLevel);
		});


		$("#gameStatusMessage").on("click", function() {
			teardownGame(map);
			initGame(map, currentLevel);
		});

		initGame(map, currentLevel);
});  